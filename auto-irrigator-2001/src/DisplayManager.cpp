//
// Created by jure on 1/14/22.
//

#include "DisplayManager.h"

// Private functions declarations
void time_formatter(uint8_t hour, uint8_t minute);

LiquidCrystal_I2C lcd(LCD_I2C_ADDRESS, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

void dm_init() {
    lcd.begin(16, 2);
    lcd.home();
    lcd.print("Auto-Irrigator");  
    lcd.setCursor(0, 1);
    lcd.print("2001    by JureD"); 
}

void dm_display_home(uint8_t hour, uint8_t minute, uint8_t next_hour, uint8_t next_minute) {
    lcd.clear();
    lcd.setCursor(5, 0);
    time_formatter(hour, minute);
    lcd.setCursor(2, 1);
    lcd.print("Next: ");
    time_formatter(next_hour, next_minute);
}

void dm_display_home_nonext(uint8_t hour, uint8_t minute) {
    lcd.clear();
    //lcd.home();
    lcd.setCursor(5, 0);
    time_formatter(hour, minute);
    lcd.setCursor(1, 1);
    lcd.print("NO NEXT TODAY!");
}

void dm_display_home_active(uint8_t hour, uint8_t minute, uint8_t current_cycle) {
    lcd.clear();
    lcd.setCursor(5, 0);
    time_formatter(hour, minute);
    lcd.setCursor(1, 1);
    lcd.print("CYCLE");
    lcd.print(current_cycle);
    lcd.print(" ACTIVE!");
}

void dm_display_set_time(uint8_t hour, uint8_t minute) {
    lcd.clear();
    lcd.home();
    lcd.print("SET CLOCK");
    lcd.setCursor(0, 1);
    time_formatter(hour, minute);
}
void dm_display_WC(uint8_t cycle_num, uint8_t hour, uint8_t minute, uint8_t length, uint8_t active) {
    lcd.clear();
    lcd.home();
    lcd.print("CYCLE ");
    lcd.print(cycle_num);

    if (active) {
        lcd.setCursor(14, 0);
        lcd.print("ON");
    } else {
        lcd.setCursor(13, 0);
        lcd.print("OFF");
    }
    
    lcd.setCursor(0, 1);
    time_formatter(hour, minute);
    
    lcd.setCursor(6, 1);
    lcd.print(length);
}


void dm_display_WC_add() {
    lcd.clear();
    lcd.home();
    lcd.print("ADD NEW");
    lcd.setCursor(1, 1);
    lcd.print("WATERING CYCLE");
}

void dm_display_error(uint8_t error_code) {
    lcd.clear();
    lcd.home();
    lcd.print("ERROR");
    lcd.setCursor(0, 1);
    lcd.print("code: ");
    lcd.print(error_code);
}

// Private functions
void time_formatter(uint8_t hour, uint8_t minute) {
    if (minute > 9) {
        lcd.print(hour);
        lcd.print(":");
        lcd.print(minute);
    } else {
        lcd.print(hour);
        lcd.print(":0");
        lcd.print(minute);
    }
}