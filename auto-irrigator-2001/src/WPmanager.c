//
// Created by jure on 1/13/22.
//

#include "WPmanager.h"

// Private function declarations
uint8_t checkTime(uint8_t cycle_hour, uint8_t cycle_minute, uint8_t cycle_length, uint8_t current_hour, uint8_t current_minute);
uint8_t readEEPROM();
void updateEEPROM(uint8_t cycle_num);

struct WateringPlan watering_plan;

void wp_init() {
    //watering_plan.cycle_counter = readEEPROM();
    watering_plan.cycle_counter = 0;

    WateringCycle (*new_WC_p) = (WateringCycle*) malloc(sizeof(WateringCycle));
    WateringCycle (**new_WP_cycles_arr_p) = malloc(sizeof(WateringCycle*));
    new_WP_cycles_arr_p[0] = new_WC_p;
    watering_plan.cycles = new_WP_cycles_arr_p;
}

void wp_add_cycle() {
    watering_plan.cycle_counter++;
    watering_plan.cycles = realloc(watering_plan.cycles, watering_plan.cycle_counter * sizeof(WateringCycle*));
    WateringCycle (*new_WC_p) = malloc(sizeof(WateringCycle));
    watering_plan.cycles[watering_plan.cycle_counter] = new_WC_p;
    watering_plan.cycles[watering_plan.cycle_counter]->hour = 0;
    watering_plan.cycles[watering_plan.cycle_counter]->minute = 0;
    watering_plan.cycles[watering_plan.cycle_counter]->length = 0;
    watering_plan.cycles[watering_plan.cycle_counter]->active = 0;
}
void wp_set_cycle_time(uint8_t cycle_num, uint8_t hour, uint8_t minute) {
    watering_plan.cycles[cycle_num]->hour = hour;
    watering_plan.cycles[cycle_num]->minute = minute;
}

uint8_t wp_get_cycle_hour(uint8_t cycle_num) {
    return watering_plan.cycles[cycle_num]->hour;
}

uint8_t wp_get_cycle_minute(uint8_t cycle_num) {
    return watering_plan.cycles[cycle_num]->minute;
}

uint8_t wp_get_cycle_length(uint8_t cycle_num) {
    return watering_plan.cycles[cycle_num]->length;
}

uint8_t wp_get_cycle_active(uint8_t cycle_num) {
    return watering_plan.cycles[cycle_num]->active;
}

void wp_set_cycle_length(uint8_t cycle_num, uint8_t length) {
    watering_plan.cycles[cycle_num]->length = length;
}

void wp_make_cycle_active(uint8_t cycle_num) {
    watering_plan.cycles[cycle_num]->active = 1;
}

void wp_make_cycle_unactive(uint8_t cycle_num) {
    watering_plan.cycles[cycle_num]->active = 0;
}

void wp_toggle_cycle_active(uint8_t cycle_num) {
    if (watering_plan.cycles[cycle_num]->active) {
        wp_make_cycle_unactive(cycle_num);
    } else {
        wp_make_cycle_active(cycle_num);
    }
}

void wp_remove_cycle(uint8_t cycle_num) {
    // Free memory holding WC data
    free(watering_plan.cycles[cycle_num]);

    // Shift WP cycle array to the left
    for (uint8_t cycle = cycle_num; cycle < watering_plan.cycle_counter; cycle++) {
        watering_plan.cycles[cycle] = watering_plan.cycles[cycle + 1];
    }

    // Decrement WP cycle counter
    watering_plan.cycle_counter--;

    // Shrink WP cycle array
    watering_plan.cycles = realloc(watering_plan.cycles, watering_plan.cycle_counter * sizeof(WateringCycle*));

}

uint8_t wp_check_if_time_to_water(uint8_t hour, uint8_t minute) {
    for (uint8_t cycle = 0; cycle <= watering_plan.cycle_counter; cycle++) {
        uint8_t cycle_hour = watering_plan.cycles[cycle]->hour;
        uint8_t cycle_minute = watering_plan.cycles[cycle]->minute;
        uint8_t cycle_length = watering_plan.cycles[cycle]->length;
        if (checkTime(cycle_hour, cycle_minute, cycle_length, hour, minute) && watering_plan.cycles[cycle]->active) {
            return cycle + 1;
        }
    }
    return 0;
}

uint8_t wp_get_next_WC(uint8_t hour, uint8_t minute) {
    uint8_t next_cycle = 0;
    uint16_t minimum_time_difference = 65535;
    uint16_t current_day_minute = hour * 60 + minute;

    for (uint8_t cycle = 0; cycle <= watering_plan.cycle_counter; cycle++) {
        if (watering_plan.cycles[cycle]->active) {
            uint16_t day_minute_next = watering_plan.cycles[cycle]->hour * 60 + watering_plan.cycles[cycle]->minute;
            int16_t time_difference = day_minute_next - current_day_minute;
            if ((time_difference > 0) && (time_difference < minimum_time_difference)) {
                next_cycle = cycle + 1;
                minimum_time_difference = time_difference;

            }
        }
    }

    return next_cycle;
}

// Private function definitions
uint8_t checkTime(uint8_t cycle_hour, uint8_t cycle_minute, uint8_t cycle_length, uint8_t current_hour, uint8_t current_minute) {
    uint16_t target_day_minute = cycle_hour * 60 + cycle_minute;
    uint16_t current_day_minute = current_hour * 60 + current_minute;
    int16_t time_difference = current_day_minute - target_day_minute;
    if ((0 <= time_difference) && (time_difference < cycle_length)) {
        return 1;
    } else {
        return 0;
    }
}