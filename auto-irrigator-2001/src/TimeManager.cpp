#include "TimeManager.h"

RTC_DS3231 rtc;

void tm_init() {
    if (! rtc.begin()) {
    dm_display_error(1);
    while (1) delay(10);
  }
}

void tm_get_time(Clock *clock) {
    DateTime now = rtc.now();
    uint8_t previous_minute = clock->minute;

    clock->hour = now.hour();
    clock->minute = now.minute();

    if (clock->minute != previous_minute) {
        clock->update = 1;
    } else {
        clock->update = 0;
    }
}

void tm_update_rtc(uint8_t hour, uint8_t minute) {
    rtc.adjust(DateTime(2021, 1, 1, hour, minute, 1));
}