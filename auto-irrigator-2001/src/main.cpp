#include <Arduino.h>

#include "main.h"

State state;

void setup() {
  dm_init();
  tm_init();
  IO_INIT
  wp_init();
  state_init();
  delay(1000);
}

void loop() {
  state_update();
  check_if_time_is_updated();
  delay(150);
}

void state_init() {
  state.step = 0;
  state.display_num = 0;
  state.max_display_num = 3;
  state.edit_mode = 0;
  state.update = 1;
}

void state_update() {
  // Get time
  tm_get_time(&(state.time));
  state.update |= state.time.update;
  // Get current inputs
  io_read_inputs(&(state.inputs));
}

void check_if_time_is_updated() {
  if (state.update) {
    check_if_time_to_water();
  } else {
    make_screen();
  }
}

void check_if_time_to_water() {
  uint8_t time_to_water = wp_check_if_time_to_water(state.time.hour, state.time.minute);
  if (time_to_water) {
    digitalWrite(VALVE_PIN, 1);
    make_screen();
  } else {
    digitalWrite(VALVE_PIN, 0);
    make_screen();
  }
}

void make_screen() {
  // Home screen
  if (state.display_num == 0) {
    make_home_screen();
  }
  // Add new WC screen
  else if (state.display_num == (state.max_display_num - 1)) {
    make_add_wc_screen();
  }
  // Set clock screen
  else if (state.display_num == (state.max_display_num)) {
    make_set_clock_screen();
  }
  // WC screens
  else {
    make_wc_screen();
  }
}

void make_home_screen() {
  uint8_t active_cycle = wp_check_if_time_to_water(state.time.hour, state.time.minute);
  if (active_cycle) {
    if (state.update) {
      dm_display_home_active(state.time.hour, state.time.minute, active_cycle - 1);
      state.update = 0;
    }
  }

  uint8_t next_cycle = wp_get_next_WC(state.time.hour, state.time.minute);
  if (next_cycle) {
    if (state.update) {
      dm_display_home(
        state.time.hour,
        state.time.minute,
        wp_get_cycle_hour(next_cycle - 1),
        wp_get_cycle_minute(next_cycle - 1)
      );
      state.update = 0;
    }
  } else {
    if (state.update) {
      dm_display_home_nonext(state.time.hour, state.time.minute);
      state.update = 0;
    }
  }

  if (state.inputs.up_btn) {
    state.display_num = 1;
    state.update = 1;
  }
  if (state.inputs.down_btn) {
    state.display_num = state.max_display_num;
    state.update = 1;
  }
}

void make_add_wc_screen() {
  if (state.update) {
    dm_display_WC_add();
    state.update = 0;
  }
  
  if (state.inputs.ok_btn) {
    state.max_display_num++;
    wp_add_cycle();
    state.update = 1;
  }
  if (state.inputs.up_btn) {
    state.display_num = state.max_display_num;
    state.update = 1;
  }
  if (state.inputs.down_btn) {
    state.display_num = state.max_display_num - 2;
    state.update = 1;
  }
}

void make_set_clock_screen() {
  uint8_t current_hour;
  uint8_t current_minute;
  switch (state.edit_mode)
  {
  case 0:
    current_hour = state.time.hour;
    current_minute = state.time.minute;
    break;
  
  case 1:
    current_hour = state.aux_val;
    current_minute = state.time.minute;
    break;
  
  case 2:
    current_hour = state.time.hour;
    current_minute = state.aux_val;
    break;
  }

  if (state.update) {
    dm_display_set_time(current_hour, current_minute);
    state.update = 0;
  }
  
  switch (state.edit_mode) {
    case 0: {
      digitalWrite(HOUR_LED_PIN, 0);
      digitalWrite(MINUTE_LED_PIN, 0);
      digitalWrite(DURATION_LED_PIN, 0);

      if (state.inputs.ok_btn) {
        state.aux_val = state.time.hour;
        state.edit_mode = 1;
        state.update = 1;
      }
      if (state.inputs.up_btn) {
        state.display_num = 0;
        state.update = 1;
      }
      if (state.inputs.down_btn) {
        state.display_num = state.max_display_num - 1;
        state.update = 1;
      }
      break;
    }
    
    case 1: {
      digitalWrite(HOUR_LED_PIN, 1);
      digitalWrite(MINUTE_LED_PIN, 0);
      digitalWrite(DURATION_LED_PIN, 0);

      if (state.inputs.ok_btn) {
        tm_update_rtc(state.aux_val, current_minute);
        state.aux_val = state.time.minute;
        state.edit_mode = 2;
        state.update = 1;
      }

      if (state.inputs.up_btn) {
        if (state.aux_val < 23) {
          state.aux_val++;
        } else {
          state.aux_val = 0;
        }

        state.update = 1;
      }

      if (state.inputs.down_btn) {
        if (state.aux_val > 0) {
          state.aux_val--;
        } else {
          state.aux_val = 23;
        }

        state.update = 1;
      }
      break;
    }
    
    case 2: {
      digitalWrite(HOUR_LED_PIN, 0);
      digitalWrite(MINUTE_LED_PIN, 1);
      digitalWrite(DURATION_LED_PIN, 0);

      if (state.inputs.ok_btn) {
        tm_update_rtc(current_hour, state.aux_val);
        state.edit_mode = 0;
        state.update = 1;
      }

      if (state.inputs.up_btn) {
        if (state.aux_val < 59) {
          state.aux_val++;
        } else {
          state.aux_val = 0;
        }

        state.update = 1;
      }

      if (state.inputs.down_btn) {
        if (state.aux_val > 0) {
          state.aux_val--;
        } else {
          state.aux_val = 59;
        }

        state.update = 1;
      }
      break;
    }
  }
}

void make_wc_screen() {
  uint8_t current_WC = state.display_num - 1;

  uint8_t current_WC_hour;
  uint8_t current_WC_minute;
  uint8_t current_WC_length;
  uint8_t current_WC_active;

  switch (state.edit_mode) {
    case 0: {
      current_WC_hour = wp_get_cycle_hour(current_WC);
      current_WC_minute = wp_get_cycle_minute(current_WC);
      current_WC_length = wp_get_cycle_length(current_WC);
      current_WC_active = wp_get_cycle_active(current_WC);
      break;
    }
    
    case 1: {
      current_WC_hour = state.aux_val;
      current_WC_minute = wp_get_cycle_minute(current_WC);
      current_WC_length = wp_get_cycle_length(current_WC);
      current_WC_active = wp_get_cycle_active(current_WC);
      break;
    }
    
    case 2: {
      current_WC_hour = wp_get_cycle_hour(current_WC);
      current_WC_minute = state.aux_val;
      current_WC_length = wp_get_cycle_length(current_WC);
      current_WC_active = wp_get_cycle_active(current_WC);
      break;
    }
    
    case 3: {
      current_WC_hour = wp_get_cycle_hour(current_WC);
      current_WC_minute = wp_get_cycle_minute(current_WC);
      current_WC_length = state.aux_val;
      current_WC_active = wp_get_cycle_active(current_WC);
      break;
    }
    
    case 4: {
      current_WC_hour = wp_get_cycle_hour(current_WC);
      current_WC_minute = wp_get_cycle_minute(current_WC);
      current_WC_length = wp_get_cycle_length(current_WC);
      current_WC_active = state.aux_val;
      break;
    }
  }
  if (state.update) {
    dm_display_WC(
      current_WC,
      current_WC_hour,
      current_WC_minute,
      current_WC_length,
      current_WC_active
    );
    state.update = 0;
  }
  

  switch (state.edit_mode) {
    case 0: {
      digitalWrite(HOUR_LED_PIN, 0);
      digitalWrite(MINUTE_LED_PIN, 0);
      digitalWrite(DURATION_LED_PIN, 0);

      if (state.inputs.ok_btn) {
        state.aux_val = current_WC_hour;
        state.edit_mode = 1;
        state.update = 1;
      }
      if (state.inputs.up_btn) {
        state.display_num++;
        state.update = 1;
      }
      if (state.inputs.down_btn) {
        state.display_num--;
        state.update = 1;
      }
      if (state.inputs.delete_btn) {
        if (state.max_display_num > 3) {
          state.max_display_num--;
          wp_remove_cycle(current_WC);
          state.update = 1;
        }
      }

      break;
    }
    
    case 1: {
      digitalWrite(HOUR_LED_PIN, 1);
      digitalWrite(MINUTE_LED_PIN, 0);
      digitalWrite(DURATION_LED_PIN, 0);

      if (state.inputs.ok_btn) {
        wp_set_cycle_time(current_WC, state.aux_val, current_WC_minute);
        state.aux_val = current_WC_minute;
        state.edit_mode = 2;
        state.update = 1;
      }
      
      if (state.inputs.up_btn) {
        if (state.aux_val < 23) {
          state.aux_val++;
        } else {
          state.aux_val = 0;
        }
        state.update = 1;
      }
      
      if (state.inputs.down_btn) {
        if (state.aux_val > 0) {
          state.aux_val--;
        } else {
          state.aux_val = 23;
        }
        state.update = 1;
      }

      break;
    }
    
    case 2: {
      digitalWrite(HOUR_LED_PIN, 0);
      digitalWrite(MINUTE_LED_PIN, 1);
      digitalWrite(DURATION_LED_PIN, 0);
      
      if (state.inputs.ok_btn) {
        wp_set_cycle_time(current_WC, current_WC_hour, state.aux_val);
        state.aux_val = current_WC_length;
        state.edit_mode = 3;
        state.update = 1;
      }
      
      if (state.inputs.up_btn) {
        if (state.aux_val < 59) {
          state.aux_val++;
        } else {
          state.aux_val = 0;
        }
        state.update = 1;
      }
      
      if (state.inputs.down_btn) {
        if (state.aux_val > 0) {
          state.aux_val--;
        } else {
          state.aux_val = 59;
        }
        state.update = 1;
      }

      break;
    }
    
    case 3: {
      digitalWrite(HOUR_LED_PIN, 0);
      digitalWrite(MINUTE_LED_PIN, 0);
      digitalWrite(DURATION_LED_PIN, 1);
      
      if (state.inputs.ok_btn) {
        wp_set_cycle_length(current_WC, state.aux_val);
        state.aux_val = current_WC_active;
        state.edit_mode = 4;
        state.update = 1;
      }
      
      if (state.inputs.up_btn) {
        if (state.aux_val < 254) {
          state.aux_val++;
        } else {
          state.aux_val = 0;
        }
        state.update = 1;
      }
      
      if (state.inputs.down_btn) {
        if (state.aux_val > 0) {
          state.aux_val--;
        } else {
          state.aux_val = 254;
        }
        state.update = 1;
      }

      break;
    }
    
    case 4: {
      digitalWrite(HOUR_LED_PIN, 1);
      digitalWrite(MINUTE_LED_PIN, 1);
      digitalWrite(DURATION_LED_PIN, 1);
      
      if (state.inputs.ok_btn) {
        if (state.aux_val) {
          wp_make_cycle_active(current_WC);
        } else {
          wp_make_cycle_unactive(current_WC);
        }
        state.edit_mode = 0;
        state.update = 1;
      }
      
      if (state.inputs.up_btn) {
        state.aux_val = 1;
        state.update = 1;
      }
      
      if (state.inputs.down_btn) {
        state.aux_val = 0;
        state.update = 1;
      }

      break;
    }
  }

}
