#include "EEPROMmanager.h"

void EEPROM_get_cycle_data(uint8_t cycle_number, WateringCycle *cycle);

void EEPROM_init(WateringPlan *watering_plan) {
    uint8_t eeprom_cycle_counter = EEPROM.read(EEPROM_WC_COUNTER_ADDRESS);
    watering_plan->cycle_counter = eeprom_cycle_counter;
    
    if (eeprom_cycle_counter) {
        // Get and set values for 0th WC
        wp_set_cycle_time(0, EEPROM.read(EEPROM_WC_HOUR_ADDRESS(0)), EEPROM.read(EEPROM_WC_MINUTE_ADDRESS(0)));
        wp_set_cycle_length(0, EEPROM.read(EEPROM_WC_LENGTH_ADDRESS(0)));
        if (EEPROM_WC_ACTIVE_ADDRESS(0)) {
            wp_make_cycle_active(0);
        } else {
            wp_make_cycle_unactive(0);
        }

        // Add other WCs values
        for (uint8_t cycle = 1; cycle < eeprom_cycle_counter; cycle++) {
            wp_add_cycle();
            wp_set_cycle_time(cycle, EEPROM.read(EEPROM_WC_HOUR_ADDRESS(cycle)), EEPROM.read(EEPROM_WC_MINUTE_ADDRESS(cycle)));
            wp_set_cycle_length(cycle, EEPROM.read(EEPROM_WC_LENGTH_ADDRESS(cycle)));
            
            if (EEPROM_WC_ACTIVE_ADDRESS(cycle)) {
                wp_make_cycle_active(cycle);
            } else {
                wp_make_cycle_unactive(cycle);
            }
        }
    }

}

void EEPROM_update(uint8_t cycle_counter, uint8_t cycle_num, WateringCycle *cycle) {
    EEPROM.update(EEPROM_WC_COUNTER_ADDRESS, cycle_counter);
    EEPROM.update(EEPROM_WC_HOUR_ADDRESS(cycle_num), cycle->hour);
    EEPROM.update(EEPROM_WC_MINUTE_ADDRESS(cycle_num), cycle->minute);
    EEPROM.update(EEPROM_WC_ACTIVE_ADDRESS(cycle_num), cycle->active);
    EEPROM.update(EEPROM_WC_LENGTH_ADDRESS(cycle_num), cycle->length);

}

void EEPROM_get_cycle_data(uint8_t cycle_number, WateringCycle *cycle) {

}