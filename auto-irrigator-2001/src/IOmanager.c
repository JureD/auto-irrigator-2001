#include "IOmanager.h"

void io_read_inputs(InputState *input_state){
    input_state->ok_btn = !digitalRead(OK_BTN_PIN);
    input_state->up_btn = !digitalRead(UP_BTN_PIN);
    input_state->down_btn = !digitalRead(DOWN_BTN_PIN);
    input_state->delete_btn = !digitalRead(DELETE_BTN_PIN);
}