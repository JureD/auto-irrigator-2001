#ifndef AI_2001_TIME_MANAGER_H
#define AI_2001_TIME_MANAGER_H

#include <Arduino.h>
#include <stdint.h>
#include <RTClib.h>

#include "DisplayManager.h"

typedef struct Clock {
    uint8_t update  :   1;
    uint8_t hour    :   5;
    uint8_t minute  :   6;
} Clock;

void tm_init();
void tm_get_time(Clock *clock);
void tm_update_rtc(uint8_t hour, uint8_t minute);

#endif //AI_2001_TIME_MANAGER_H