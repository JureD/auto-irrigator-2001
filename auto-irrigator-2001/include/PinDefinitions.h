#ifndef AI_2001_PIN_DEFINITIONS_H
#define AI_2001_PIN_DEFINITIONS_H

// Inputs
#define OK_BTN_PIN 6
#define UP_BTN_PIN 4
#define DOWN_BTN_PIN 7
#define DELETE_BTN_PIN 3

// Outputs
#define HOUR_LED_PIN 10
#define MINUTE_LED_PIN 9
#define DURATION_LED_PIN 5
#define VALVE_PIN 8

// LCD display
#define LCD_I2C_ADDRESS 0x27




#endif //AI_2001_PIN_DEFINITIONS_H