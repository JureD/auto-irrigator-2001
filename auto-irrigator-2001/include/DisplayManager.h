#ifndef AI_2001_DISPLAY_MANAGER_H
#define AI_2001_DISPLAY_MANAGER_H

#include <Arduino.h>
#include <stdint.h>
#include <stdio.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include "PinDefinitions.h"


void dm_init();
void dm_display_home(uint8_t hour, uint8_t minute, uint8_t next_hour, uint8_t next_minute);
void dm_display_home_nonext(uint8_t hour, uint8_t minute);
void dm_display_home_active(uint8_t hour, uint8_t minute, uint8_t current_cycle);
void dm_display_set_time(uint8_t hour, uint8_t minute);
void dm_display_WC(uint8_t cycle_num, uint8_t hour, uint8_t minute, uint8_t length, uint8_t active);
void dm_display_WC_add();
void dm_display_error(uint8_t error_code);



#endif //AI_2001_DISPLAY_MANAGER_H