#ifndef AI_2001_IO_MANAGER_H
#define AI_2001_IO_MANAGER_H

#include <Arduino.h>
#include <stdint.h>

#include "PinDefinitions.h"


typedef struct InputState {
    uint8_t ok_btn   :   1;
    uint8_t up_btn   :   1;
    uint8_t down_btn :   1;
    uint8_t delete_btn:  1;
} InputState;

typedef struct OutputState {
    uint8_t valve       :   1;
    uint8_t hour_led    :   1;
    uint8_t minute_led  :   1;
    uint8_t duration_led:   1;
} OutputState;

#define IO_INIT {\
    pinMode(OK_BTN_PIN, INPUT_PULLUP);  \
    pinMode(UP_BTN_PIN, INPUT_PULLUP);  \
    pinMode(DOWN_BTN_PIN, INPUT_PULLUP);\
    pinMode(DELETE_BTN_PIN, INPUT_PULLUP);\
    pinMode(HOUR_LED_PIN, OUTPUT);\
    pinMode(MINUTE_LED_PIN, OUTPUT);\
    pinMode(DURATION_LED_PIN, OUTPUT);\
    pinMode(VALVE_PIN, OUTPUT);\
}

void io_read_inputs(InputState *input_state);




#endif //AI_2001_IO_MANAGER_H