#ifndef AI_2001_MAIN_H
#define AI_2001_MAIN_H

#include <Arduino.h>
#include <stdint.h>

#include "TimeManager.h"
#include "PinDefinitions.h"
#include "DisplayManager.h"
extern "C" {
  #include "IOmanager.h"
  #include "WPmanager.h"
}

typedef struct State {
  Clock time;
  InputState inputs;
  uint8_t step;
  uint8_t display_num       : 4;
  uint8_t max_display_num   : 4;
  uint8_t edit_mode         : 3;
  uint8_t update            : 1;
  uint8_t aux_val;
} State;

void state_init();
void state_update();
void make_steps();

// Steps
void check_if_time_is_updated();
void check_if_time_to_water();
void make_screen();
void make_home_screen();
void make_add_wc_screen();
void make_set_clock_screen();
void make_wc_screen();

#endif //AI_2001_MAIN_H