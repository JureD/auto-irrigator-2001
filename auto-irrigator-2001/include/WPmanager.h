//
// Created by jure on 1/13/22.
//

#ifndef AI_2001_WATERINGPLAN_H
#define AI_2001_WATERINGPLAN_H

#include <stdlib.h>
#include <stdint.h>

//typedef struct WateringCycle WateringCycle;
//typedef struct WaterinPlan WateringPlan;
typedef struct WateringCycle {
    uint8_t active  :   1;
    uint8_t hour    :   5;
    uint8_t minute  :   6;
    uint8_t length;
} WateringCycle;

typedef struct WateringPlan {
    uint8_t cycle_counter;
    WateringCycle** cycles;
} WateringPlan;

void wp_init();
void wp_add_cycle();
void wp_set_cycle_time(uint8_t cycle_num, uint8_t hour, uint8_t minute);
uint8_t wp_get_cycle_hour(uint8_t cycle_num);
uint8_t wp_get_cycle_minute(uint8_t cycle_num);
uint8_t wp_get_cycle_length(uint8_t cycle_num);
uint8_t wp_get_cycle_active(uint8_t cycle_num);
void wp_set_cycle_length(uint8_t cycle_num, uint8_t length);
void wp_make_cycle_active(uint8_t cycle_num);
void wp_make_cycle_unactive(uint8_t cycle_num);
void wp_toggle_cycle_active(uint8_t cycle_num);
void wp_remove_cycle(uint8_t cycle_num);
/*
 * Function returns 0 if it is not time to water.
 * If it is time to water function returns number+1 of WC that is watering (If WC-0 is active return is 1).
*/
uint8_t wp_check_if_time_to_water(uint8_t hour, uint8_t minute);
uint8_t wp_get_next_WC(uint8_t hour, uint8_t minute);


#endif //AI_2001_WATERINGPLAN_H
